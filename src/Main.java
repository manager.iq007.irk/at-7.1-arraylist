import java.util.*;

public class Main {
    public static void main(String[] args) {

        //Задать массив
        String[] colorsArray = {"red", "orange", "yellow", "green", "blue", "purple", "pink"};

        //сделать массив листом
        List<String> colorsList = Arrays.asList(colorsArray);

        Iterator<String> iterator = colorsList.iterator();
        while (iterator.hasNext()) {
            String color = iterator.next();
            System.out.println("Вывод итератором: " + color);
        }

        Collections.sort(colorsList);
        System.out.println("Сортировка colorsList:\n " + colorsList);

        //замена элементов
        colorsList.set(0, "dark " + colorsList.get(0));
        colorsList.set(2, "dark " + colorsList.get(2));
        colorsList.set(4, "dark " + colorsList.get(4));

        //вывести на экран циклом for-each
        for (String color : colorsList) {
            System.out.println(color);
        }

        //получить подсписок с 1 по 5 элементы включительно
        List<String> colorsList2 = new ArrayList<>(colorsList.subList(0, 7));
        System.out.println("Подсписок с 1 по 5 элементы - " + colorsList2.subList(0, 5));

        //метод, который поменяет местами 1 и 4 элементы между собой.
        Collections.swap(colorsList2, 0, 3);

        String a1 = colorsList2.get(3);
        System.out.println("Объект из переменной а1 содержится в коллекции? - " + colorsList2.contains(a1));

        //Удалить все элементы, содержащие букву 'o'
        colorsList2.removeIf(i -> i.contains("o"));

        // превратить список в массив
        String[] colorsAgainArray = new String[colorsList2.size()];
        colorsList2.toArray(colorsAgainArray);
        System.out.println("Новый массив - " + Arrays.toString(colorsAgainArray));
    }

}